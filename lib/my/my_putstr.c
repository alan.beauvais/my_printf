/*
** EPITECH PROJECT, 2019
** my_putstr.c
** File description:
** Task02 of the day03
*/

#include <stdarg.h>

int my_putchar(char c);

int print_str(va_list list)
{
    return my_putstr(va_arg(list, char *));
}

int my_putstr(char const *str)
{
    int i = 0;

    while (str[i] != '\0') {
        my_putchar(str[i]);
        i++;
    }
    return i;
}
