##
## EPITECH PROJECT, 2019
## Makefile
## File description:
## My own makefile
##

SRC =	./lib/my/my_getnbr.c \
		./lib/my/my_printf.c \
		./lib/my/my_put_nbr.c \
		./lib/my/my_put_double.c \
		./lib/my/my_putchar.c \
		./lib/my/my_putstr.c \
		./lib/my/my_strlen.c \
		./lib/my/my_nbr_to_base.c \
		./lib/my/my_revstr.c \
		./lib/my/my_strtolower.c \
		./lib/my/my_isneg.c \
		./lib/my/my_put_ptr.c \
		./lib/my/my_strtoupper.c

OBJ =	$(SRC:.c=.o)

NAME =	libmy.a

all:	$(NAME)

$(NAME):	$(OBJ)
	ar rc $(NAME) $(OBJ)
clean:
	rm -f $(OBJ) a.out

fclean:	clean
	rm -f $(NAME) a.out

re:	fclean all
